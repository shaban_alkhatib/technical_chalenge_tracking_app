package com.floow.trackme.entities;

import android.arch.persistence.room.Embedded;
import android.arch.persistence.room.Relation;
import java.util.List;

/**
 * Contains the list of location points in a specific recorded journey
 */
public class JourneyDetails {

    @Embedded private Journey journey;
    @Relation(parentColumn = "id", entityColumn = "journeyId", entity = LocationPoint.class)
    private List<LocationPoint> locationPoints;

    public JourneyDetails() {}

    public Journey getJourney() {
        return journey;
    }

    public void setJourney(Journey journey) {
        this.journey = journey;
    }

    public List<LocationPoint> getLocationPoints() {
        return locationPoints;
    }

    public void setLocationPoints(List<LocationPoint> locationPoints) {
        this.locationPoints = locationPoints;
    }
}
