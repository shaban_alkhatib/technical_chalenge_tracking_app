package com.floow.trackme.entities;

import android.arch.persistence.room.Entity;
import android.arch.persistence.room.Ignore;
import android.arch.persistence.room.PrimaryKey;
import android.arch.persistence.room.TypeConverters;
import com.floow.trackme.tools.DateConverter;
import java.util.Date;

/**
 * Contains main information about a specific recorded journey
 */
@Entity(tableName = "Journeys")
@TypeConverters(DateConverter.class)
public class Journey {

    @PrimaryKey(autoGenerate = true) private long id;
    private Date startDate, endDate;
    private String name;

    public Journey(){}

    @Ignore
    public Journey(Date startDate, Date endDate) {
        this.startDate = startDate;
        this.endDate = endDate;
    }

    public long getId() {
        return id;
    }

    public void setId(long id) {
        this.id = id;
    }

    public Date getStartDate() {
        return startDate;
    }

    public void setStartDate(Date startDate) {
        this.startDate = startDate;
    }

    public Date getEndDate() {
        return endDate;
    }

    public void setEndDate(Date endDate) {
        this.endDate = endDate;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }
}
