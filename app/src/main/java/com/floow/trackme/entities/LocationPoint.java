package com.floow.trackme.entities;

import android.arch.persistence.room.Entity;
import android.arch.persistence.room.Ignore;
import android.arch.persistence.room.PrimaryKey;
import android.arch.persistence.room.TypeConverters;
import android.location.Location;
import com.floow.trackme.tools.Constants;
import com.floow.trackme.tools.LocationConverter;

/**
 * Represents a single recorded location
 */
@Entity(tableName = Constants.TABLE_LOCATIONS)
public class LocationPoint {

    @PrimaryKey(autoGenerate = true) private long id;
    private long journeyId;
    @TypeConverters(LocationConverter.class) private Location location;

    public LocationPoint() {}

    @Ignore
    public LocationPoint(long journeyId,  Location location) {
        this.location = location;
        this.journeyId = journeyId;
    }

    public long getId() {
        return id;
    }

    public void setId(long id) {
        this.id = id;
    }

    public Location getLocation() {
        return location;
    }

    public void setLocation(Location location) {
        this.location = location;
    }

    public long getJourneyId() {
        return journeyId;
    }

    public void setJourneyId(long journeyId) {
        this.journeyId = journeyId;
    }
}
