package com.floow.trackme.viewmodels;

import android.app.Application;
import android.arch.lifecycle.AndroidViewModel;
import android.arch.lifecycle.LiveData;
import com.floow.trackme.TrackMeApplication;
import com.floow.trackme.contracts.JourneysListContract;
import com.floow.trackme.dao.TrackerDao;
import com.floow.trackme.entities.Journey;
import java.util.List;
import javax.inject.Inject;

/**
 * Created by Shaban on 11/18/2017.
 * View Model to decouple journey list view from entities
 */
public class JourneyListViewModel extends AndroidViewModel implements JourneysListContract.Model{

    @Inject TrackerDao dao;

    public JourneyListViewModel(Application application) {
        super(application);
        ((TrackMeApplication) application).getAppComponent().inject(this);
    }

    @Override
    public LiveData<List<Journey>> getJourneys() {
        return dao.getJourneys();
    }

}


