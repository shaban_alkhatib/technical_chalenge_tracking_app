package com.floow.trackme.viewmodels;

import android.app.Application;
import android.arch.lifecycle.AndroidViewModel;
import android.arch.lifecycle.LiveData;
import com.floow.trackme.TrackMeApplication;
import com.floow.trackme.contracts.JourneyDetailsContract;
import com.floow.trackme.dao.TrackerDao;
import com.floow.trackme.entities.JourneyDetails;
import javax.inject.Inject;

/**
 * Created by Shaban on 11/18/2017.
 * View Model to decouple journey path view from entities
 */

public class JourneyDetailsViewModel extends AndroidViewModel implements JourneyDetailsContract.Model {

    @Inject TrackerDao dao;

    public JourneyDetailsViewModel(Application application) {
        super(application);
        ((TrackMeApplication) application).getAppComponent().inject(this);
    }

    @Override
    public LiveData<JourneyDetails> getJourneyDetails(long journeyId) {
        return dao.getJourneyByID(journeyId);
    }

}


