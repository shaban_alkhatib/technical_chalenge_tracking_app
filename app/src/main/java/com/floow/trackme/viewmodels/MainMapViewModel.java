package com.floow.trackme.viewmodels;

import android.app.Application;
import android.arch.lifecycle.AndroidViewModel;
import android.arch.lifecycle.LiveData;
import android.content.SharedPreferences;
import com.floow.trackme.TrackMeApplication;
import com.floow.trackme.contracts.MainMapContract;
import com.floow.trackme.dao.DBHelper;
import com.floow.trackme.dao.TrackerDao;
import com.floow.trackme.entities.Journey;
import com.floow.trackme.entities.JourneyDetails;
import com.floow.trackme.entities.LocationPoint;
import com.floow.trackme.listeners.OnJourneyCreateListener;
import javax.inject.Inject;

/**
 * Created by Shaban on 11/18/2017.
 * View Model to decouple Main Map view from entities
 */
public class MainMapViewModel extends AndroidViewModel implements MainMapContract.Model {

    @Inject TrackerDao dao;
    @Inject SharedPreferences preferences;

    public MainMapViewModel(Application application) {
        super(application);
        ((TrackMeApplication) application).getAppComponent().inject(this);
    }

    @Override
    public void addJourney(long startTime, OnJourneyCreateListener listener) {
        new DBHelper.NewJourneyTask(startTime, preferences, dao, listener).execute();
    }

    @Override
    public void addLocation(LocationPoint locationPoint) {
        new DBHelper.NewLocationTask(locationPoint, dao).execute();
    }

    @Override
    public void finishJourney(Journey journey) {
        new DBHelper.UpdateJourneyTask(journey, preferences, dao).execute();
    }

    @Override
    public LiveData<JourneyDetails> getJourneyDetails(long id) {
        return dao.getJourneyByID(id);
    }

}
