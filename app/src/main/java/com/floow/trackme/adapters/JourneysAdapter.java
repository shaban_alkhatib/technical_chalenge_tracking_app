package com.floow.trackme.adapters;

import android.content.Context;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;
import com.floow.trackme.R;
import com.floow.trackme.entities.Journey;
import com.floow.trackme.listeners.OnJourneyClickListener;
import com.floow.trackme.tools.Utils;
import java.util.List;

/**
 * Created by Shaban on 11/20/2017.
 * handles journeys list cards
 */

public class JourneysAdapter extends RecyclerView.Adapter<JourneysAdapter.JourneyHolder> {

    private Context context;
    private List<Journey> journeys;
    private OnJourneyClickListener listener;

    public JourneysAdapter(Context context) {
        this.context = context;
    }

    @Override
    public JourneyHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        final View view = LayoutInflater.from(parent.getContext()).inflate(R.layout.card_journey,
                parent, false);
        return new JourneyHolder(view);
    }

    @Override
    public void onBindViewHolder(JourneyHolder holder, int position) {
        final Journey journey = journeys.get(position);
        holder.tvName.setText(journey.getName());
        String dates = Utils.formatDate(journey.getStartDate()) + " " + context.getString(R.string.to)
                + " " + Utils.formatDate(journey.getEndDate());
        holder.tvDate.setText(dates);
        holder.itemView.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                if (listener !=  null)
                    listener.onClick(journey);
            }
        });
    }

    @Override
    public int getItemCount() {
        return journeys == null ? 0 : journeys.size();
    }

    //Holder class of a journey card
    class JourneyHolder extends RecyclerView.ViewHolder {

        private TextView tvName, tvDate;

        JourneyHolder(View view) {
            super(view);
            tvName = view.findViewById(R.id.card_name_tv);
            tvDate = view.findViewById(R.id.card_dates_tv);
        }
    }

    // Refresh the adapter after setting the data
    public void setJourneys(List<Journey> journeys) {
        this.journeys = journeys;
        Utils.runOnMainThread(context, new Runnable() {
            @Override
            public void run() {
                JourneysAdapter.this.notifyDataSetChanged();
            }
        });
    }

    public void setListener(OnJourneyClickListener listener) {
        this.listener = listener;
    }
}
