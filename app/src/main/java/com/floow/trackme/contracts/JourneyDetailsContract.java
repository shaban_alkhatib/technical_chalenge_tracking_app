package com.floow.trackme.contracts;

import android.arch.lifecycle.LifecycleOwner;
import android.arch.lifecycle.LiveData;
import com.floow.trackme.entities.JourneyDetails;
import com.floow.trackme.entities.LocationPoint;
import com.google.android.gms.maps.OnMapReadyCallback;
import com.google.android.gms.maps.model.CameraPosition;
import com.google.android.gms.maps.model.Marker;
import com.google.android.gms.maps.model.PolylineOptions;

import java.util.List;

/**
 * Created by Shaban on 11/21/2017.
 * Interfaces for MVP pattern of Journey Details activity
 */

public class JourneyDetailsContract {
    public interface View extends LifecycleOwner, OnMapReadyCallback {
        void receiveIntent();
        void showStartPosition(LocationPoint point, CameraPosition.Builder builder);
        void showEndPosition(LocationPoint point);
        void drawPath(List<LocationPoint> points, CameraPosition.Builder builder, PolylineOptions options);
        void showInformation(Marker marker, String title, String message);
    }

    public interface Presenter {
        void loadPath();
        void onStop();
        void setStartMarker(Marker startMarker);
        void onMarkerClicked(Marker marker);
    }

    public interface Model {
        LiveData<JourneyDetails> getJourneyDetails(long journeyId);
    }
}
