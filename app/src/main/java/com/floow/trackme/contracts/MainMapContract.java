package com.floow.trackme.contracts;

import android.arch.lifecycle.LifecycleOwner;
import android.arch.lifecycle.LiveData;
import com.floow.trackme.entities.Journey;
import com.floow.trackme.entities.JourneyDetails;
import com.floow.trackme.entities.LocationPoint;
import com.floow.trackme.listeners.OnJourneyCreateListener;
import com.google.android.gms.maps.OnMapReadyCallback;
import com.google.android.gms.maps.model.CameraPosition;
import com.google.android.gms.maps.model.LatLng;
import com.google.android.gms.maps.model.PolylineOptions;

import java.util.List;

/**
 * Created by Shaban on 11/18/2017.
 * Contract of available operations between View-Presenter and Presenter-Model for Main Map activity
 */

public class MainMapContract {
    public interface View extends LifecycleOwner, OnMapReadyCallback {
        void showLocationSettingsDialog();
        void showCurrentLocation(LatLng location, CameraPosition.Builder builder);
        void drawPath(List<LatLng> locationPoints, PolylineOptions options, CameraPosition.Builder builder);
        void drawPointsPath(List<LocationPoint> locationPoints, PolylineOptions options, CameraPosition.Builder builder);
        void extendPath(LatLng location, PolylineOptions options, CameraPosition.Builder builder);
        void setTrackingState(boolean active);
        void silentStopTracking();
        void clearGoogleMap();
        void showListSnackBar();
        void showSnackBar(String message, int duration);
        void hideSnackBar();
        void goToListActivity();
        void startService();
        void stopService();
    }

    public interface Presenter {
        void registerReceiver();
        void unregisterReceiver();
        void setLiveDataListener();
        void startTracking();
        void stopTracking();
        void startDetectingLocation();
        void stopDetectingLocation();
        void listButtonClicked();
        void switchToggled(boolean checked);
        void getTrackingState();
        void checkGPS();
        void clearData();
        void onDestroy();
    }

    public interface Model {
        void addJourney(long startTime, OnJourneyCreateListener listener);
        void addLocation(LocationPoint locationPoint);
        void finishJourney(Journey journey);
        LiveData<JourneyDetails> getJourneyDetails(long journeyId);
    }
}
