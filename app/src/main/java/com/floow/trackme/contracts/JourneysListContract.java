package com.floow.trackme.contracts;

import android.arch.lifecycle.LifecycleOwner;
import android.arch.lifecycle.LiveData;
import com.floow.trackme.adapters.JourneysAdapter;
import com.floow.trackme.entities.Journey;
import java.util.List;

/**
 * Created by Shaban on 11/21/2017.
 * Interfaces for MVP pattern of Journeys List activity
 */

public class JourneysListContract {
    public interface View extends LifecycleOwner {
        void setAdapter(JourneysAdapter adapter);
        void finishActivity();
        void pause(boolean startClicked);
        void goToJourneyPathActivity(long journeyId);
        void showEmptyLayout();
        void hideEmptyLayout();
    }

    public interface Presenter {
        void loadJourneys();
        void onStartClicked();
        void onPause();
        void loadAdapter();
    }

    public interface Model {
        LiveData<List<Journey>> getJourneys();
    }
}
