package com.floow.trackme.activities;

import android.Manifest;
import android.app.Activity;
import android.arch.lifecycle.ViewModelProviders;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.pm.PackageManager;
import android.os.Bundle;
import android.provider.Settings;
import android.support.annotation.NonNull;
import android.support.design.widget.Snackbar;
import android.support.v4.app.FragmentActivity;
import android.support.v7.app.AlertDialog;
import android.view.View;
import android.widget.CompoundButton;
import android.widget.FrameLayout;
import android.widget.ImageButton;
import android.widget.Switch;
import com.floow.trackme.R;
import com.floow.trackme.TrackMeApplication;
import com.floow.trackme.entities.LocationPoint;
import com.floow.trackme.contracts.MainMapContract;
import com.floow.trackme.presenters.MainMapPresenter;
import com.floow.trackme.services.TrackerService;
import com.floow.trackme.tools.Constants;
import com.floow.trackme.tools.MapUtils;
import com.floow.trackme.tools.Utils;
import com.floow.trackme.viewmodels.MainMapViewModel;
import com.google.android.gms.maps.GoogleMap;
import com.google.android.gms.maps.SupportMapFragment;
import com.google.android.gms.maps.model.CameraPosition;
import com.google.android.gms.maps.model.LatLng;
import com.google.android.gms.maps.model.PolylineOptions;
import java.util.List;
import butterknife.BindView;
import butterknife.ButterKnife;

/**
 * Created by Shaban on 11/19/2017.
 * UI Screen to show your location on map and draw your path at real time
 * with ability to record a path as a journey
 */
public class MainMapActivity extends FragmentActivity implements MainMapContract.View{

    @BindView(R.id.root_main) FrameLayout layoutRoot;
    @BindView(R.id.switch_track_main) Switch switchTracking;
    @BindView(R.id.btn_list_main) ImageButton btnList;
    private Intent serviceIntent;
    private GoogleMap googleMap;
    private MainMapContract.Presenter mainMapPresenter;
    private Snackbar snackbar;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main_map);
        //Bind views
        ButterKnife.bind(this);
        //Get teh Mao fragment contained in the activity layout
        SupportMapFragment mapFragment = (SupportMapFragment) getSupportFragmentManager()
                .findFragmentById(R.id.map);
        //Initialize the intent for Tracking Service
        serviceIntent = new Intent(MainMapActivity.this, TrackerService.class);
        //Initialize the presenter of this activity with the view model related to this screen
        mainMapPresenter = new MainMapPresenter((TrackMeApplication) getApplication(), this,
                ViewModelProviders.of(MainMapActivity.this).get(MainMapViewModel.class));
        //Check if the app is in recording state or not and change the switch state according to that
        mainMapPresenter.getTrackingState();
        initEvents();
        //Get the GoogleMap object asynchronously
        mapFragment.getMapAsync(this);
        //Show a message if Google Play Services are missed from the device
        if (!Utils.isGooglePlayServicesAvailable(this)) {
            showSnackBar(getString(R.string.services_missed), Snackbar.LENGTH_INDEFINITE);
        }
    }

    @Override
    public void onMapReady(GoogleMap googleMap) {
        this.googleMap = googleMap;
    }

    private void initEvents() {
        switchTracking.setOnCheckedChangeListener(new CompoundButton.OnCheckedChangeListener() {
            @Override
            public void onCheckedChanged(CompoundButton compoundButton, boolean active) {
                mainMapPresenter.switchToggled(active);
            }
        });
        btnList.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                mainMapPresenter.listButtonClicked();
            }
        });
    }

    //Check if location permission is granted to start fetching location
    @Override
    protected void onResume() {
        super.onResume();
        if (Utils.isPermissionGranted(this, Manifest.permission.ACCESS_FINE_LOCATION, Constants.FINE_LOCATION_REQUEST)) {
            mainMapPresenter.checkGPS();
        }
    }

    @Override
    protected void onStop() {
        mainMapPresenter.stopDetectingLocation();
        super.onStop();
    }

    //When receiving a permission response, if location permission is granted than start the checkGPS process
    @Override
    public void onRequestPermissionsResult(int requestCode, @NonNull String[] permissions, @NonNull int[] grantResults) {
        super.onRequestPermissionsResult(requestCode, permissions, grantResults);
        if (requestCode == Constants.FINE_LOCATION_REQUEST) {
            if (grantResults[0] == PackageManager.PERMISSION_GRANTED)
                mainMapPresenter.checkGPS();
            else {
                finish();
            }
        }
    }

    //When returning from changing location settings
    @Override
    protected void onActivityResult(int requestCode, int resultCode, Intent data) {
        super.onActivityResult(requestCode, resultCode, data);
        if (requestCode == Constants.REQUEST_SETTINGS) {
            if (resultCode == Activity.RESULT_OK) {
                mainMapPresenter.startDetectingLocation();
            }
        }
    }

    //Show a dialog that leads to location settings page if location is not activated
    @Override
    public void showLocationSettingsDialog() {
        final AlertDialog.Builder dialog = new AlertDialog.Builder(this);
        dialog.setMessage(getString(R.string.enable_gps));
        dialog.setPositiveButton(getString(R.string.go_to_settings), new DialogInterface.OnClickListener() {
            @Override
            public void onClick(DialogInterface paramDialogInterface, int paramInt) {
                Intent myIntent = new Intent(Settings.ACTION_LOCATION_SOURCE_SETTINGS);
                startActivityForResult(myIntent, Constants.REQUEST_SETTINGS);
            }
        });
        dialog.setNegativeButton(getString(R.string.close), new DialogInterface.OnClickListener() {
            @Override
            public void onClick(DialogInterface dialogInterface, int i) {
                dialogInterface.dismiss();
            }
        });
        dialog.show();
    }

    //Show current location on map as magenta circle
    @Override
    public void showCurrentLocation(LatLng location, CameraPosition.Builder builder) {
        MapUtils.showCurrentLocation(googleMap, location, builder);
    }

    @Override
    public void drawPath(List<LatLng> locationPoints, PolylineOptions options, CameraPosition.Builder builder) {
        MapUtils.drawPath(googleMap, options, locationPoints, builder);
    }

    //To draw a new path on map
    @Override
    public void drawPointsPath(List<LocationPoint> locationPoints, PolylineOptions options, CameraPosition.Builder builder) {
        MapUtils.drawPointsPath(googleMap, options, locationPoints, builder);
    }

    //To extend am existing path on map
    @Override
    public void extendPath(LatLng location, PolylineOptions options, CameraPosition.Builder builder) {
        MapUtils.extendPath(googleMap, options, location, builder);
    }


    @Override
    public void setTrackingState(boolean active) {
        switchTracking.setChecked(active);
    }

    //Toggle off the switch without triggering any event
    @Override
    public void silentStopTracking() {
        switchTracking.setOnCheckedChangeListener(null);
        switchTracking.setChecked(false);
        initEvents();
    }

    //Remove current drawn paths, circles, markers .. on the map
    @Override
    public void clearGoogleMap() {
        if (googleMap != null)
            googleMap.clear();
    }

    //Show a bar at the bottom of screen with an action that leads to the list activity to see the new journey path
    @Override
    public void showListSnackBar() {
        snackbar = Snackbar.make(layoutRoot, getString(R.string.journey_added), Snackbar.LENGTH_LONG);
        snackbar.setAction(getString(R.string.view_list), new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                goToListActivity();
            }
        });
        snackbar.show();
    }

    //Show a bar at the bottom with a specific message without actions
    @Override
    public void showSnackBar(String message, int duration) {
        snackbar = Snackbar.make(layoutRoot, message, duration);
        snackbar.show();
    }

    //Hide any shown snack bars
    @Override
    public void hideSnackBar() {
        if (snackbar != null && snackbar.isShown())
            snackbar.dismiss();
    }

    //Open the activity of recorded journeys list
    @Override
    public void goToListActivity() {
        Intent intent = new Intent(MainMapActivity.this, JourneysListActivity.class);
        startActivity(intent);
    }

    //Start the Tracking service when resume the activity
    @Override
    public void startService() {
        startService(serviceIntent);
    }

    //top the Tracking service when recording of a specific journey is ended
    // or when closing the app without enabling recording
    @Override
    public void stopService() {
        stopService(serviceIntent);
    }

    @Override
    protected void onDestroy() {
        mainMapPresenter.onDestroy();
        super.onDestroy();
    }
}
