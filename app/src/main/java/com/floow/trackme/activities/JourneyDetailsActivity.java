package com.floow.trackme.activities;

import android.arch.lifecycle.ViewModelProviders;
import android.os.Bundle;
import android.support.v4.app.FragmentActivity;
import com.floow.trackme.R;
import com.floow.trackme.TrackMeApplication;
import com.floow.trackme.entities.LocationPoint;
import com.floow.trackme.contracts.JourneyDetailsContract;
import com.floow.trackme.presenters.JourneyDetailsPresenter;
import com.floow.trackme.tools.Constants;
import com.floow.trackme.tools.MapUtils;
import com.floow.trackme.tools.Utils;
import com.floow.trackme.viewmodels.JourneyDetailsViewModel;
import com.google.android.gms.maps.GoogleMap;
import com.google.android.gms.maps.MapView;
import com.google.android.gms.maps.MapsInitializer;
import com.google.android.gms.maps.model.CameraPosition;
import com.google.android.gms.maps.model.LatLng;
import com.google.android.gms.maps.model.Marker;
import com.google.android.gms.maps.model.PolylineOptions;
import java.util.List;
import butterknife.BindView;
import butterknife.ButterKnife;

/**
 * Created by Shaban on 11/19/2017.
 * UI Screen to show a recorded journey path
 */

public class JourneyDetailsActivity extends FragmentActivity implements JourneyDetailsContract.View{

    @BindView(R.id.journey_map) MapView mapView;
    private GoogleMap map;
    private long journeyId;
    private JourneyDetailsPresenter journeyDetailsPresenter;
    private boolean resumed, mapInitialized = false;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_journey_details);
        //Bind views
        ButterKnife.bind(this);
        //Create the map view
        mapView.onCreate(savedInstanceState);
        //Get the current journey id sent from previous screen
        receiveIntent();
        //Initialize the presenter of this activity with the view model related to this screen
        journeyDetailsPresenter = new JourneyDetailsPresenter((TrackMeApplication) getApplication(), this,
                ViewModelProviders.of(JourneyDetailsActivity.this).get(JourneyDetailsViewModel.class), journeyId);
        //Get the GoogleMap object asynchronously
        mapView.getMapAsync(this);
    }

    //Get the current journey id sent from previous screen
    @Override
    public void receiveIntent() {
        Bundle bundle = getIntent().getExtras();
        if (bundle != null && bundle.containsKey(Constants.JOURNEY_ID)) {
            journeyId = bundle.getLong(Constants.JOURNEY_ID);
        }
    }

    //Initialize tha map when it is ready and set markers listener
    @Override
    public void onMapReady(GoogleMap googleMap) {
        this.map = googleMap;
        MapsInitializer.initialize(this);
        resumeMap();
        map.setOnMarkerClickListener(new GoogleMap.OnMarkerClickListener() {
            @Override
            public boolean onMarkerClick(Marker marker) {
                journeyDetailsPresenter.onMarkerClicked(marker);
                return true;
            }
        });
    }

    //Resume the Map view only one time
    private synchronized void resumeMap() {
        if (resumed)
            mapView.onResume();
        mapInitialized = true;
        journeyDetailsPresenter.loadPath();
    }

    //Draw the start location related to the selected journey
    @Override
    public void showStartPosition(LocationPoint point, CameraPosition.Builder builder) {
        LatLng latLng = Utils.getLatLngFromLocation(point.getLocation());
        Marker marker = MapUtils.showStartLocation(map, latLng, builder);
        journeyDetailsPresenter.setStartMarker(marker);
    }

    //Draw the end location related to the selected journey
    @Override
    public void showEndPosition(LocationPoint point) {
        LatLng latLng = Utils.getLatLngFromLocation(point.getLocation());
        MapUtils.showEndLocation(map, latLng);
    }

    //Draw the path of locations related to the selected journey
    @Override
    public void drawPath(List<LocationPoint> points, CameraPosition.Builder builder, PolylineOptions polylineOptions) {
        List<LatLng> latLngs = Utils.getLatLngsFromLocationPoints(points);
        if (latLngs.size() > 0)
            MapUtils.drawPath(map, polylineOptions, latLngs, null);
    }


    //Show a title and snippet when clicking on a Marker
    @Override
    public void showInformation(Marker marker, String title, String message) {
        marker.setTitle(title);
        marker.setSnippet(message);
        marker.showInfoWindow();
    }

    //Apply activity lifecycle changes on the map view

    @Override
    protected void onResume() {
        super.onResume();
        resumed = true;
        if (mapView != null && mapInitialized)
            resumeMap();
    }

    @Override
    protected void onDestroy() {
        super.onDestroy();
        if (mapView != null)
            mapView.destroyDrawingCache();
    }

    @Override
    protected void onPause() {
        super.onPause();
        if (mapView != null)
            mapView.onPause();
    }

    @Override
    protected void onStop() {
        super.onStop();
        if (mapView != null)
            mapView.onStop();
        journeyDetailsPresenter.onStop();
    }

    @Override
    public void onLowMemory() {
        super.onLowMemory();
        if (mapView != null)
            mapView.onLowMemory();
    }


}
