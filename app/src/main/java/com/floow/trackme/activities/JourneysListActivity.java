package com.floow.trackme.activities;

import android.arch.lifecycle.ViewModelProviders;
import android.content.Intent;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.RecyclerView;
import android.view.View;
import android.widget.Button;
import android.widget.LinearLayout;
import android.widget.ProgressBar;
import com.floow.trackme.R;
import com.floow.trackme.TrackMeApplication;
import com.floow.trackme.adapters.JourneysAdapter;
import com.floow.trackme.presenters.JourneysListPresenter;
import com.floow.trackme.contracts.JourneysListContract;
import com.floow.trackme.tools.Constants;
import com.floow.trackme.tools.Utils;
import com.floow.trackme.viewmodels.JourneyListViewModel;
import butterknife.BindView;
import butterknife.ButterKnife;

/**
 * Created by Shaban on 11/19/2017.
 * UI Screen to show a list of recorded journeys
 * with ability to rename, delete or view the path on map
 */

public class JourneysListActivity extends AppCompatActivity implements JourneysListContract.View {

    @BindView(R.id.list_recycler) RecyclerView recyclerView;
    @BindView(R.id.list_empty_layout) LinearLayout layoutEmpty;
    @BindView(R.id.list_start_btn) Button startButton;
    @BindView(R.id.list_progress) ProgressBar progressBar;
    private JourneysListPresenter journeysListPresenter;

    @Override
    protected void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_journeys_list);
        //Bind views
        ButterKnife.bind(this);
        //Initialize the presenter of this activity with the view model related to this screen
        journeysListPresenter = new JourneysListPresenter((TrackMeApplication) getApplication(), this,
                ViewModelProviders.of(JourneysListActivity.this).get(JourneyListViewModel.class));
        journeysListPresenter.loadAdapter();
        Utils.configureToolbar(this);
        initEvents();
        //Loading the list of recorded journeys
        journeysListPresenter.loadJourneys();
    }

    private void initEvents() {
        startButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                journeysListPresenter.onStartClicked();
            }
        });
    }

    //Handle toolbar arrow click to go back
    @Override
    public boolean onSupportNavigateUp() {
        onBackPressed();
        return true;
    }

    @Override
    protected void onPause() {
        super.onPause();
        journeysListPresenter.onPause();
    }

    //Opening a new Map for a selected journey
    @Override
    public void goToJourneyPathActivity(long journeyId) {
        Intent intent = new Intent(JourneysListActivity.this, JourneyDetailsActivity.class);
        intent.putExtra(Constants.JOURNEY_ID, journeyId);
        startActivity(intent);
    }

    //To show a layout when there's no recorded journeys yet
    @Override
    public void showEmptyLayout() {
        layoutEmpty.setVisibility(View.VISIBLE);
    }

    @Override
    public void hideEmptyLayout() {
        layoutEmpty.setVisibility(View.GONE);
    }

    @Override
    public void setAdapter(JourneysAdapter adapter) {
        recyclerView.setAdapter(adapter);
    }

    @Override
    public void finishActivity() {
        finish();
    }

    @Override
    public void pause(boolean startClicked) {
        if (startClicked)
            overridePendingTransition(0, 0);
    }
}
