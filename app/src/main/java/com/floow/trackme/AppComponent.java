package com.floow.trackme;

import com.floow.trackme.adapters.JourneysAdapter;
import com.floow.trackme.modules.AppModule;
import com.floow.trackme.modules.DaoModule;
import com.floow.trackme.modules.ListModule;
import com.floow.trackme.modules.MapModule;
import com.floow.trackme.modules.NamesModule;
import com.floow.trackme.presenters.JourneyDetailsPresenter;
import com.floow.trackme.presenters.JourneysListPresenter;
import com.floow.trackme.presenters.MainMapPresenter;
import com.floow.trackme.services.TrackerService;
import com.floow.trackme.viewmodels.JourneyDetailsViewModel;
import com.floow.trackme.viewmodels.JourneyListViewModel;
import com.floow.trackme.viewmodels.MainMapViewModel;

import javax.inject.Singleton;
import dagger.Component;

/**
 * Created by Shaban on 11/16/2017.
 * App Component to inject needed modules in targeted objects
 */
@Singleton
@Component(modules = {AppModule.class, DaoModule.class, ListModule.class, MapModule.class, NamesModule.class})
public interface AppComponent{
    //Presenters
    void inject(MainMapPresenter mainMapPresenter);
    void inject(JourneyDetailsPresenter journeyDetailsPresenter);
    void inject(JourneysListPresenter journeysListPresenter);
    //Models
    void inject(JourneyListViewModel journeyListViewModel);
    void inject(JourneyDetailsViewModel detailsViewModel);
    void inject(MainMapViewModel mainMapViewModel);
    //Service
    void inject(TrackerService trackerService);
    //List Adapter
    void inject(JourneysAdapter journeysAdapter);
}
