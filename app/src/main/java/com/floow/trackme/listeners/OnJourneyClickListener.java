package com.floow.trackme.listeners;

import com.floow.trackme.entities.Journey;

/**
 * Created by Shaban on 11/20/2017.
 * Interface of journey card operations
 */

public interface OnJourneyClickListener {
    void onClick(Journey journey);
}
