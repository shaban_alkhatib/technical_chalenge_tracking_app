package com.floow.trackme.listeners;

/**
 * Created by Shaban on 11/18/2017.
 * Interface of journey creation event
 */

public interface OnJourneyCreateListener {

    void onJourneyCreated(long journeyId);

}
