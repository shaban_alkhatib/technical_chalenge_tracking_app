package com.floow.trackme.presenters;

import android.arch.lifecycle.LiveData;
import android.arch.lifecycle.Observer;
import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.Intent;
import android.content.IntentFilter;
import android.content.SharedPreferences;
import android.location.Location;
import android.location.LocationManager;
import android.support.annotation.Nullable;
import android.support.design.widget.Snackbar;
import android.support.v4.content.LocalBroadcastManager;
import android.util.Log;
import com.floow.trackme.R;
import com.floow.trackme.TrackMeApplication;
import com.floow.trackme.contracts.MainMapContract;
import com.floow.trackme.entities.Journey;
import com.floow.trackme.entities.JourneyDetails;
import com.floow.trackme.entities.LocationPoint;
import com.floow.trackme.listeners.OnJourneyCreateListener;
import com.floow.trackme.tools.Constants;
import com.floow.trackme.tools.Utils;
import com.google.android.gms.maps.model.CameraPosition;
import com.google.android.gms.maps.model.LatLng;
import com.google.android.gms.maps.model.PolylineOptions;
import java.util.Date;
import java.util.List;
import javax.inject.Inject;

/**
 * Created by Shaban on 11/20/2017.
 * A presenter that play the role of managing Main Map views and models
 */

public class MainMapPresenter extends BroadcastReceiver implements  MainMapContract.Presenter{

    @Inject PolylineOptions polylineOptions;
    @Inject SharedPreferences preferences;
    @Inject CameraPosition.Builder positionBuilder;
    @Inject LocalBroadcastManager localBroadcastManager;
    @Inject LocationManager locationManager;
    @Inject List<LatLng> locationPoints;
    private TrackMeApplication application;
    private MainMapContract.View mainMapView;
    private MainMapContract.Model mainMapViewModel;
    private boolean firstBroadcast = true;
    private LiveData<JourneyDetails> liveData;

    public MainMapPresenter(TrackMeApplication application, MainMapContract.View mainMapView,
                            MainMapContract.Model mainMapViewModel) {
        this.mainMapView = mainMapView;
        this.application = application;
        //inject needed objects
        application.getAppComponent().inject(this);
        this.mainMapViewModel = mainMapViewModel;
    }

    /*To register a broadcast receiver that receive location changes from the service to be shown on the map
      It is used in tracking off state*/
    @Override
    public void registerReceiver() {
        mainMapView.clearGoogleMap();
        clearData();
        IntentFilter filter = new IntentFilter(Constants.LOCATION_BROADCAST_ACTION);
        localBroadcastManager.registerReceiver(this, filter);
    }

    // unregister the broadcast receiver of the tracking service when start tracking
    @Override
    public void unregisterReceiver() {
        localBroadcastManager.unregisterReceiver(this);
    }

    /* LiveData used toc reflect the changes on database to the related views on screen
       It is used instead of broadcast receiver in tacking on state.
       When a new location added to database an event triggered here to show the new location on map*/
    @Override
    public void setLiveDataListener() {
        long journeyId = Utils.getCurrentJourneyID(preferences);
        if (journeyId > 0) {
            liveData = mainMapViewModel.getJourneyDetails(journeyId);
            liveData.observe(mainMapView, new Observer<JourneyDetails>() {
                @Override
                public void onChanged(@Nullable JourneyDetails journeyDetails) {
                    if (journeyDetails != null){
                        mainMapView.clearGoogleMap();
                        Log.e("LiveData", journeyDetails.getLocationPoints().size() + "");
                        List<LocationPoint> locationPointList = journeyDetails.getLocationPoints();
                        if (locationPointList.size() > 0) {
                            LocationPoint last = locationPointList.get(locationPointList.size() - 1);
                            LatLng loc = new LatLng(last.getLocation().getLatitude(), last.getLocation().getLongitude());
                            if ((polylineOptions.getPoints() == null || polylineOptions.getPoints().size() == 0)
                                    && journeyDetails.getLocationPoints().size() > 1) {
                                mainMapView.drawPointsPath(locationPointList, polylineOptions, positionBuilder);
                            } else {
                                mainMapView.extendPath(loc, polylineOptions, positionBuilder);
                            }
                            mainMapView.showCurrentLocation(loc, positionBuilder);
                        }
                    }
                }
            });
        }
    }

    /* When switching on tracking, it checks the GPS and Network Provider again
       Then it insert a new journey to database with current time and last detected location as a starting point
       it unregisters the broadcast receiver
    */
    @Override
    public void startTracking() {
        if (Utils.isGPSEnabled(locationManager) || Utils.isNetworkEnabled(locationManager)) {
            if (liveData != null)
                liveData.removeObservers(mainMapView);
            mainMapViewModel.addJourney(System.currentTimeMillis(), new OnJourneyCreateListener() {
                @Override
                public void onJourneyCreated(long journeyId) {
                    if (locationPoints != null) {
                        long journeyID = Utils.getCurrentJourneyID(preferences);
                        Log.e("JOURNEY_ADDED", journeyID + "");
                        if (journeyID != 0) {
                            setLiveDataListener();
                            if (locationPoints.size() > 0) {
                                Location location = new Location("");
                                location.setLatitude(locationPoints.get(locationPoints.size() - 1).latitude);
                                location.setLongitude(locationPoints.get(locationPoints.size() - 1).longitude);
                                clearData();
                                mainMapViewModel.addLocation(new LocationPoint(journeyID, location));
                            } else {
                                LatLng latLng = Utils.getLastLocation(preferences);
                                if (latLng != null) {
                                    Location location = new Location("");
                                    location.setLatitude(latLng.latitude);
                                    location.setLongitude(latLng.longitude);
                                    clearData();
                                    mainMapViewModel.addLocation(new LocationPoint(journeyID, location));
                                }
                            }
                            mainMapView.showSnackBar(application.getString(R.string.new_journey_started), Snackbar.LENGTH_SHORT);
                        }
                    }
                }
            });
            unregisterReceiver();
        } else {
            mainMapView.showLocationSettingsDialog();
            mainMapView.silentStopTracking();
        }
    }

    /* When switching tracking off the last inserted journey will be updated to set the end time
       It register back the broadcast receiver */
    @Override
    public void stopTracking() {
        mainMapView.clearGoogleMap();
        clearData();
        if (liveData != null)
            liveData.removeObservers(mainMapView);
        registerReceiver();
        long journeyId = Utils.getCurrentJourneyID(preferences);
        if (journeyId != 0) {
            Journey journey = new Journey(new Date(Utils.getCurrentJourneyStartDate(preferences)),
                    new Date(System.currentTimeMillis()));
            journey.setId(journeyId);
            journey.setName(application.getString(R.string.journey) + " " + journeyId);
            mainMapViewModel.finishJourney(journey);
            mainMapView.showListSnackBar();
        }
    }

    /* When activity is resumed and the app is in recording state, enable LiveData listeners
       or register a receiver if recording is off
       It shows the current location and the current path when recording is on*/
    @Override
    public void startDetectingLocation() {
        if (Utils.isTrackingOn(preferences)) {
            if (!Utils.isServiceRunning(application))
                mainMapView.startService();
            setLiveDataListener();
        } else {
            clearData();
            LatLng latLng = Utils.getLastLocation(preferences);
            mainMapView.startService();
            registerReceiver();
            mainMapView.clearGoogleMap();
            if (latLng != null)
                mainMapView.showCurrentLocation(latLng, positionBuilder);
            else
                mainMapView.showSnackBar(application.getString(R.string.fetching_location), Snackbar.LENGTH_INDEFINITE);
        }
    }

    // When closing the app, unregister receiver and stop tracking service in tracking off state
    // or stop listening to database changes but keep the service running in the background
    @Override
    public void stopDetectingLocation() {
        if (!Utils.isTrackingOn(preferences)) {
            localBroadcastManager.unregisterReceiver(this);
            mainMapView.stopService();
        } else {
            if (liveData != null)
                liveData.removeObservers(mainMapView);
        }
    }

    @Override
    public void switchToggled(boolean checked) {
        Utils.setTracking(preferences, checked);
        if (checked) {
            startTracking();
        } else
            stopTracking();
    }

    @Override
    public void listButtonClicked() {
        mainMapView.goToListActivity();
    }

    /* When the receiver receive a new location from the Tracking Service
       It shows tha path and current location on map*/
    @Override
    public void onReceive(Context context, Intent intent) {
        if (intent.getExtras() != null && intent.getExtras().containsKey(Constants.LOCATION_RESULT)) {
            String result = intent.getExtras().getString(Constants.LOCATION_RESULT);
            if (result != null && result.equals(Constants.LOCAL)){
                LatLng latLng = Utils.getLastLocation(preferences);
                mainMapView.clearGoogleMap();
                if (latLng != null) {
                    locationPoints.add(latLng);
                    if (locationPoints.size() == 2) {
                        mainMapView.drawPath(locationPoints, polylineOptions, positionBuilder);
                    } else if (locationPoints.size() > 2){
                        mainMapView.extendPath(latLng, polylineOptions, positionBuilder);
                    }
                    mainMapView.showCurrentLocation(latLng, positionBuilder);
                    if (firstBroadcast)
                        mainMapView.hideSnackBar();
                    else
                        firstBroadcast = false;
                }
            }
        }
    }

    @Override
    public void getTrackingState() {
        mainMapView.setTrackingState(Utils.isTrackingOn(preferences));
    }

    @Override
    public void checkGPS() {
        if (Utils.isGPSEnabled(locationManager) || Utils.isNetworkEnabled(locationManager))
            startDetectingLocation();
        else
            mainMapView.showLocationSettingsDialog();
    }

    @Override
    public void clearData() {
        if (locationPoints != null)
            locationPoints.clear();
        if (polylineOptions != null && polylineOptions.getPoints() != null)
            polylineOptions.getPoints().clear();
    }

    @Override
    public void onDestroy() {
        if (!Utils.isTrackingOn(preferences))
            mainMapView.stopService();
    }
}
