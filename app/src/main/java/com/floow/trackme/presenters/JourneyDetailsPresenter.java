package com.floow.trackme.presenters;

import android.arch.lifecycle.LiveData;
import android.arch.lifecycle.Observer;
import android.support.annotation.Nullable;
import com.floow.trackme.R;
import com.floow.trackme.TrackMeApplication;
import com.floow.trackme.contracts.JourneyDetailsContract;
import com.floow.trackme.entities.JourneyDetails;
import com.floow.trackme.tools.Utils;
import com.google.android.gms.maps.model.CameraPosition;
import com.google.android.gms.maps.model.Marker;
import com.google.android.gms.maps.model.PolylineOptions;
import java.util.Date;
import javax.inject.Inject;

/**
 * Created by Shaban on 11/21/2017.
 * A presenter class that play the role of controller between Journey details map view and models
 */

public class JourneyDetailsPresenter implements JourneyDetailsContract.Presenter {

    @Inject PolylineOptions polylineOptions;
    @Inject CameraPosition.Builder positionBuilder;
    private JourneyDetailsContract.View detailsMapView;
    private JourneyDetailsContract.Model detailsViewModel;
    private long journeyId;
    private TrackMeApplication application;
    private LiveData<JourneyDetails> liveData;
    private Marker startMarker;
    private Date startDate, endDate;

    public JourneyDetailsPresenter(TrackMeApplication application, JourneyDetailsContract.View detailsMapView,
                                   JourneyDetailsContract.Model detailsViewModel, long journeyId) {
        this.detailsMapView = detailsMapView;
        this.application = application;
        //inject needed objects
        application.getAppComponent().inject(this);
        this.detailsViewModel = detailsViewModel;
        this.journeyId = journeyId;
    }

    /* Get all location points related to the selected journey and ask the view to draw the path
       and markers of Starting and ending points with snippets showing the time*/
    @Override
    public void loadPath() {
        liveData = detailsViewModel.getJourneyDetails(journeyId);
        liveData.observe(detailsMapView, new Observer<JourneyDetails>() {
            @Override
            public void onChanged(@Nullable JourneyDetails journeyDetails) {
                if (journeyDetails != null && journeyDetails.getLocationPoints() != null){
                    detailsMapView.drawPath(journeyDetails.getLocationPoints(), positionBuilder, polylineOptions);
                    if (journeyDetails.getLocationPoints().size() > 0) {
                        detailsMapView.showEndPosition(journeyDetails.getLocationPoints()
                                .get(journeyDetails.getLocationPoints().size() - 1));
                        detailsMapView.showStartPosition(journeyDetails.getLocationPoints().get(0), positionBuilder);
                        startDate = journeyDetails.getJourney().getStartDate();
                        endDate = journeyDetails.getJourney().getEndDate();
                    }
                }
            }
        });
    }

    @Override
    public void setStartMarker(Marker startMarker) {
        this.startMarker = startMarker;
    }

    @Override
    public void onStop() {
        if (liveData != null)
            liveData.removeObservers(detailsMapView);
    }

    //Ask the view to show the window info of the clicked marker
    @Override
    public void onMarkerClicked(Marker marker) {
        if (startMarker.equals(marker)) {
            detailsMapView.showInformation(marker, application.getString(R.string.startingPoint),
                    Utils.formatDate(startDate));
        } else {
            detailsMapView.showInformation(marker, application.getString(R.string.endingPoint),
                    Utils.formatDate(endDate));
        }
    }


}
