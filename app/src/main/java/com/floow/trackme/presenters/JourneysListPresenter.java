package com.floow.trackme.presenters;

import android.arch.lifecycle.Observer;
import android.support.annotation.Nullable;
import com.floow.trackme.TrackMeApplication;
import com.floow.trackme.adapters.JourneysAdapter;
import com.floow.trackme.contracts.JourneysListContract;
import com.floow.trackme.entities.Journey;
import com.floow.trackme.listeners.OnJourneyClickListener;
import java.util.List;
import javax.inject.Inject;

/**
 * Created by Shaban on 11/21/2017.
 *  * A presenter that play the role of managing Journeys list views and models
 */

public class JourneysListPresenter implements JourneysListContract.Presenter {

    private JourneysListContract.View listView;
    private JourneysListContract.Model listViewModel;
    @Inject JourneysAdapter journeysAdapter;
    private boolean startClicked = false;

    public JourneysListPresenter(TrackMeApplication application, JourneysListContract.View listView,
                                 JourneysListContract.Model listViewModel) {
        this.listView = listView;
        this.listViewModel = listViewModel;
        //inject needed objects
        application.getAppComponent().inject(this);
    }

    // Get recorded journeys list from database
    @Override
    public void loadJourneys() {
        listViewModel.getJourneys().observe(listView, new Observer<List<Journey>>() {
            @Override
            public void onChanged(@Nullable List<Journey> journeys) {
                if (journeys != null){
                    journeysAdapter.setJourneys(journeys);
                    //When clicking on journey card open a new Map to show the path
                    // with starting and ending points
                    journeysAdapter.setListener(new OnJourneyClickListener() {
                        @Override
                        public void onClick(Journey journey) {
                            listView.goToJourneyPathActivity(journey.getId());
                        }
                    });
                    if (journeys.size() == 0) {
                        listView.showEmptyLayout();
                    } else {
                        listView.hideEmptyLayout();
                    }
                }
            }
        });
    }

    @Override
    public void onStartClicked() {
        startClicked = true;
        listView.finishActivity();
    }

    @Override
    public void onPause() {
        listView.pause(!startClicked);
    }

    @Override
    public void loadAdapter() {
        listView.setAdapter(journeysAdapter);
    }
}
