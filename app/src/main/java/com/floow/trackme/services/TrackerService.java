package com.floow.trackme.services;

import android.Manifest;
import android.app.Service;
import android.content.Intent;
import android.content.SharedPreferences;
import android.content.pm.PackageManager;
import android.location.Location;
import android.location.LocationManager;
import android.os.Bundle;
import android.os.IBinder;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.support.v4.app.ActivityCompat;
import android.support.v4.content.LocalBroadcastManager;
import android.util.Log;
import com.floow.trackme.R;
import com.floow.trackme.TrackMeApplication;
import com.floow.trackme.dao.DBHelper;
import com.floow.trackme.dao.TrackerDao;
import com.floow.trackme.entities.Journey;
import com.floow.trackme.entities.LocationPoint;
import com.floow.trackme.tools.Constants;
import com.floow.trackme.tools.Utils;
import com.google.android.gms.common.ConnectionResult;
import com.google.android.gms.common.api.GoogleApiClient;
import com.google.android.gms.common.api.PendingResult;
import com.google.android.gms.common.api.ResultCallback;
import com.google.android.gms.common.api.Status;
import com.google.android.gms.location.LocationListener;
import com.google.android.gms.location.LocationRequest;
import com.google.android.gms.location.LocationServices;
import com.google.android.gms.location.LocationSettingsRequest;
import com.google.android.gms.location.LocationSettingsResult;
import com.google.android.gms.location.LocationSettingsStatusCodes;
import java.util.Date;
import javax.inject.Inject;
import javax.inject.Named;

/**
 * Created by Shaban on 11/16/2017.
 * A sticky service to get user location in background
 * So it will record the path on database even if the app is closed
 */
public class TrackerService extends Service implements GoogleApiClient.ConnectionCallbacks,
        GoogleApiClient.OnConnectionFailedListener, LocationListener, android.location.LocationListener {

    @Inject LocationRequest locationRequest;
    @Inject LocationSettingsRequest locationSettingsRequest;
    @Inject GoogleApiClient.Builder googleApiClientBuilder;
    @Inject SharedPreferences preferences;
    @Inject TrackerDao database;
    @Inject @Named(Constants.LOCATION_BROADCAST) Intent broadcastIntent;
    @Inject LocalBroadcastManager localBroadcastManager;
    @Inject LocationManager locationManager;
    GoogleApiClient googleApiClient;
    private Location lastLocation;
    private boolean usingGoogleApi = false;

    @Override
    public void onCreate() {
        super.onCreate();
        ((TrackMeApplication) getApplication()).getAppComponent().inject(this);
        broadcastIntent.putExtra(Constants.LOCATION_RESULT, Constants.LOCAL);
        startLocationDetection();
    }

    //If Google Play service available start using it, or use Location Manager otherwise
    private void startLocationDetection() {
        if (Utils.isGooglePlayServicesAvailable(getApplicationContext())) {
            startUsingPlayServices();
        } else {
            startUsingLocationManager();
        }
    }

    private void startUsingPlayServices() {
        usingGoogleApi = true;
        googleApiClientBuilder.addConnectionCallbacks(this)
                .addOnConnectionFailedListener(this);
        googleApiClient = googleApiClientBuilder.build();
        googleApiClient.connect();
    }

    private void startUsingLocationManager() {
        usingGoogleApi = false;
        if (Utils.isGPSEnabled(locationManager))
            requestLocationUpdates(LocationManager.GPS_PROVIDER);
        else if (Utils.isNetworkEnabled(locationManager))
            requestLocationUpdates(LocationManager.NETWORK_PROVIDER);
        localBroadcastManager.sendBroadcast(broadcastIntent);
    }

    private void requestLocationUpdates(String provider) {
        if (ActivityCompat.checkSelfPermission(this, Manifest.permission.ACCESS_FINE_LOCATION)
                != PackageManager.PERMISSION_GRANTED
                && ActivityCompat.checkSelfPermission(this, Manifest.permission.ACCESS_COARSE_LOCATION)
                != PackageManager.PERMISSION_GRANTED) {
            return;
        }
        lastLocation = locationManager.getLastKnownLocation(provider);
        locationManager.requestLocationUpdates(provider,
                Constants.LOCATION_INTERVAL, Constants.LOCATION_MIN_DISTANCE_FOR_CHANGE, this);
    }

    //Sticky service to stay running even when app is closed
    @Override
    public int onStartCommand(Intent intent, int flags, int startId) {
        return Service.START_STICKY;
    }

    @Nullable
    @Override
    public IBinder onBind(Intent intent) {
        return null;
    }

    //When the google api is connected we can get the last location and start listening for location changes
    @Override
    public void onConnected(@Nullable Bundle bundle) {
        PendingResult<LocationSettingsResult> result =
                LocationServices.SettingsApi.checkLocationSettings(googleApiClient, locationSettingsRequest);
        result.setResultCallback(new ResultCallback<LocationSettingsResult>() {
            @Override
            public void onResult(@NonNull LocationSettingsResult result) {
                final Status status = result.getStatus();
                if (status.getStatusCode() == LocationSettingsStatusCodes.SUCCESS) {
                    if (ActivityCompat.checkSelfPermission(getApplicationContext(),
                            Manifest.permission.ACCESS_FINE_LOCATION) == PackageManager.PERMISSION_GRANTED) {
                        lastLocation = LocationServices.FusedLocationApi.getLastLocation(googleApiClient);
                        if (lastLocation != null) {
                            Utils.setLastLocation(preferences, lastLocation);
                            LocationServices.FusedLocationApi.requestLocationUpdates(googleApiClient,
                                    locationRequest, TrackerService.this);
                            localBroadcastManager.sendBroadcast(broadcastIntent);
                        }
                    }
                } else {
                    startUsingLocationManager();
                    if (googleApiClient.isConnected())
                        googleApiClient.disconnect();
                }
            }
        });
    }

    @Override
    public void onConnectionSuspended(int i) {
        Log.e("CONNECTION", "SUSPENDED");
    }

    @Override
    public void onConnectionFailed(@NonNull ConnectionResult connectionResult) {
        startUsingLocationManager();
    }

    /* When a new location is available insert it to the database under the current journey if tracking is on
       Or sending it using the broadcast manager to be received from the receiver on main map presenter and
       show the location on map with extending the path
     */
    @Override
    public void onLocationChanged(final Location location) {
        Log.e("Location Update", "Latitude " + location.getLatitude() + " Longitude " + location.getLongitude());
        Utils.setLastLocation(preferences, location);
        long journeyID = Utils.getCurrentJourneyID(preferences);
        if (Utils.isTrackingOn(preferences) && journeyID != 0) {
            new DBHelper.NewLocationTask(new LocationPoint(journeyID, location), database).execute();
        } else {
            localBroadcastManager.sendBroadcast(broadcastIntent);
        }
    }

    @Override
    public void onStatusChanged(String s, int i, Bundle bundle) {
    }

    //Handle changes in providers states
    @Override
    public void onProviderEnabled(String s) {
        if (Utils.isGPSEnabled(locationManager) && Utils.isNetworkEnabled(locationManager) && !usingGoogleApi) {
            startUsingPlayServices();
            locationManager.removeUpdates(this);
        } else if (Utils.isGPSEnabled(locationManager) ||  Utils.isNetworkEnabled(locationManager) && usingGoogleApi) {
            if (googleApiClient != null && googleApiClient.isConnected())
                googleApiClient.disconnect();
            startUsingLocationManager();
        } else if (Utils.isGPSEnabled(locationManager) && !usingGoogleApi) {
            if (googleApiClient != null && googleApiClient.isConnected())
                googleApiClient.disconnect();
            startUsingLocationManager();
        }
    }

    @Override
    public void onProviderDisabled(String s) {
        if (!Utils.isGPSEnabled(locationManager) && !Utils.isNetworkEnabled(locationManager)){
            finishJourney();
        }
    }

    @Override
    public void onDestroy() {
        if (googleApiClient != null && googleApiClient.isConnected())
            googleApiClient.disconnect();
        finishJourney();
        super.onDestroy();
    }

    //If the service ended for unexpected reason update the current journey and set the end date of it
    private void finishJourney() {
        if (Utils.getCurrentJourneyID(preferences) != 0) {
            Journey journey = new Journey(new Date(Utils.getCurrentJourneyStartDate(preferences)),
                    new Date(System.currentTimeMillis()));
            journey.setId(Utils.getCurrentJourneyID(preferences));
            journey.setName(getString(R.string.journey) + " " + journey.getId());
            new DBHelper.UpdateJourneyTask(journey, preferences, database).execute();
        }
    }
}
