package com.floow.trackme.modules;

import com.floow.trackme.tools.Constants;

import javax.inject.Named;

import dagger.Module;
import dagger.Provides;

/**
 * Created by Shaban on 11/21/2017.
 * Contains strings to be injected into targets or other modules
 */
@Module
public class NamesModule {

    @Provides
    @Named(Constants.TRACK_PREFERENCES)
    String trackingPreferencesName() {
        return Constants.TRACK_PREFERENCES_NAME;
    }

    @Provides @Named(Constants.LOCATION_ACTION)
    String locationBroadcastAction() {
        return Constants.LOCATION_BROADCAST_ACTION;
    }

}
