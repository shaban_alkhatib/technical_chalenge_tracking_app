package com.floow.trackme.modules;

import android.content.Context;
import android.content.SharedPreferences;
import com.floow.trackme.TrackMeApplication;
import com.floow.trackme.tools.Constants;
import javax.inject.Named;
import javax.inject.Singleton;
import dagger.Module;
import dagger.Provides;

/**
 * Created by Shaban on 11/16/2017.
 * Contains general app objects to be injected into targets
 */
@Module
public class AppModule {

    private final TrackMeApplication application;

    public AppModule(TrackMeApplication application) {
        this.application = application;
    }

    @Provides @Singleton
    Context provideContext() {
        return application;
    }

    @Provides @Singleton
    SharedPreferences sharedPreferences(Context context, @Named(Constants.TRACK_PREFERENCES) String name) {
        return context.getSharedPreferences(name, Context.MODE_PRIVATE);
    }

}
