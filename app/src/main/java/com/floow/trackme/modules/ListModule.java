package com.floow.trackme.modules;

import android.content.Context;

import com.floow.trackme.adapters.JourneysAdapter;

import dagger.Module;
import dagger.Provides;

/**
 * Created by Shaban on 11/21/2017.
 * Contains list-related objects to be injected into targets
 */
@Module
public class ListModule {
    @Provides
    JourneysAdapter journeysAdapter(Context context) {
        return new JourneysAdapter(context);
    }
}
