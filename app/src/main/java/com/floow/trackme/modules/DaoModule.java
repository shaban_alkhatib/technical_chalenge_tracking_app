package com.floow.trackme.modules;

import android.arch.persistence.room.Room;
import android.content.Context;

import com.floow.trackme.dao.TrackerDao;
import com.floow.trackme.dao.TrackerDatabase;
import com.floow.trackme.tools.Constants;

import javax.inject.Named;
import javax.inject.Singleton;

import dagger.Module;
import dagger.Provides;

/**
 * Created by Shaban on 11/21/2017.
 * Contains database-related objects to be injected into targets
 */
@Module
public class DaoModule {

    @Provides @Singleton
    TrackerDatabase trackerDatabase(Context context, @Named(Constants.DB_NAME) String dbName) {
        return Room.databaseBuilder(context.getApplicationContext(),
                TrackerDatabase.class, dbName).build();
    }

    @Provides @Singleton
    TrackerDao trackerDao(TrackerDatabase database) {
        return database.trackerDao();
    }

    @Provides @Named(Constants.DB_NAME)
    String databaseName() {
        return "tracker_db";
    }

}
