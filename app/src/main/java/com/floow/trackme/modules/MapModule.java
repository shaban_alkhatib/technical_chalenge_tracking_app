package com.floow.trackme.modules;

import android.content.Context;
import android.content.Intent;
import android.graphics.Color;
import android.location.LocationManager;
import android.support.v4.content.LocalBroadcastManager;
import com.floow.trackme.tools.Constants;
import com.google.android.gms.common.api.GoogleApiClient;
import com.google.android.gms.location.LocationRequest;
import com.google.android.gms.location.LocationServices;
import com.google.android.gms.location.LocationSettingsRequest;
import com.google.android.gms.maps.model.CameraPosition;
import com.google.android.gms.maps.model.LatLng;
import com.google.android.gms.maps.model.PolylineOptions;
import java.util.ArrayList;
import java.util.List;
import javax.inject.Named;
import javax.inject.Singleton;
import dagger.Module;
import dagger.Provides;

/**
 * Created by Shaban on 11/21/2017.
 * Contains map-related and location-related objects to be injected into targets
 */
@Module
public class MapModule {
    @Provides
    GoogleApiClient.Builder googleApiClient(Context context) {
        return new GoogleApiClient.Builder(context)
                .addApi(LocationServices.API);
    }

    @Provides
    @Singleton
    LocationRequest highLocationRequest() {
        LocationRequest locationRequest = new LocationRequest();
        locationRequest.setInterval(Constants.LOCATION_INTERVAL);
        locationRequest.setFastestInterval(Constants.FASTEST_LOCATION_INTERVAL);
        locationRequest.setPriority(LocationRequest.PRIORITY_HIGH_ACCURACY);
        return locationRequest;
    }

    @Provides
    @Singleton
    LocationSettingsRequest highLocationSettingsRequest( LocationRequest request) {
        LocationSettingsRequest.Builder builder = new LocationSettingsRequest.Builder()
                .addLocationRequest(request);
        return builder.build();
    }

    @Provides @Named(Constants.LOCATION_BROADCAST)
    Intent intent(@Named(Constants.LOCATION_ACTION) String action) {
        return new Intent(action);
    }

    @Provides @Singleton
    LocalBroadcastManager localBroadcastManager(Context context) {
        return LocalBroadcastManager.getInstance(context.getApplicationContext());
    }

    @Provides
    PolylineOptions polylineOptions() {
        return new PolylineOptions().width(6).color(Color.CYAN).geodesic(true);
    }
    @Provides @Singleton
    CameraPosition.Builder pathCameraPositionBuilder() {
        return new CameraPosition.Builder().zoom(17).tilt(40);
    }

    @Provides @Singleton
    LocationManager locationManager(Context context) {
        return (LocationManager) context.getSystemService(Context.LOCATION_SERVICE);
    }

    @Provides
    List<LatLng> locationPoints() {
        return new ArrayList<>();
    }

}
