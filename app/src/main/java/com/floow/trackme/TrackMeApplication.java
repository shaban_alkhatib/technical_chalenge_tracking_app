package com.floow.trackme;

import android.app.Application;
import com.floow.trackme.modules.AppModule;

/**
 * Created by Shaban on 11/17/2017.
 * Track Me Application class, used to instantiate dagger and other general configurations
 */

public class TrackMeApplication extends Application {

    private AppComponent appComponent;

    @Override
    public void onCreate() {
        super.onCreate();
        try {
            Class.forName("android.os.AsyncTask");
        } catch (ClassNotFoundException e) {
            e.printStackTrace();
        }
        appComponent = initDagger(this);
    }

    /**
     * It initialize the AppComponent object using dagger 2 to be used in the whole app
     * @param application: An instance of the TrackMeApplication
     * @return AppComponent instance to be injected in targeted objects
     */
    protected AppComponent initDagger(TrackMeApplication application) {
        return DaggerAppComponent.builder()
                .appModule(new AppModule(application))
                .build();
    }

    public AppComponent getAppComponent() {
        return appComponent;
    }
}
