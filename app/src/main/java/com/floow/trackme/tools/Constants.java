package com.floow.trackme.tools;

/**
 * Created by Shaban on 11/16/2017.
 * File of final static constants to avoid errors in typing
 */
public class Constants {

    public static final String TABLE_LOCATIONS = "Points";
    public static final String TABLE_JOURNEYS = "Journeys";
    public static final String DB_NAME = "DB_NAME";
    public static final String TRACK_PREFERENCES = "TRACK_PREFERENCES";
    public static final String TRACK_PREFERENCES_NAME = "TRACK_DATA";
    public static final String LOCATION_ACTION = "LOCATION_ACTION";
    public static final String LOCATION_BROADCAST = "LOCATION_BROADCAST";
    public static final String LOCATION_BROADCAST_ACTION = "com.floow.trackme.services.TrackerService";
    public static final String LOCATION_RESULT = "LOCATION_RESULT";
    public static final String LOCAL = "LOCAL";
    public static final int FINE_LOCATION_REQUEST = 2457;
    public static final int REQUEST_SETTINGS = 2525;
    public static final String JOURNEY_ID = "JourneyID";
    static final String SERVICE_CLASS_NAME = "com.floow.trackme.services.TrackerService";
    public static final long LOCATION_INTERVAL = 3000;
    public static final long FASTEST_LOCATION_INTERVAL = 1000;
    public static final float LOCATION_MIN_DISTANCE_FOR_CHANGE = 1f;
}
