package com.floow.trackme.tools;

import android.app.Activity;
import android.app.ActivityManager;
import android.content.Context;
import android.content.SharedPreferences;
import android.content.pm.PackageManager;
import android.location.Location;
import android.location.LocationManager;
import android.os.Build;
import android.os.Handler;
import android.support.annotation.NonNull;
import android.support.v4.app.ActivityCompat;
import android.support.v4.content.ContextCompat;
import android.support.v7.app.AppCompatActivity;
import com.floow.trackme.entities.LocationPoint;
import com.google.android.gms.common.ConnectionResult;
import com.google.android.gms.common.GoogleApiAvailability;
import com.google.android.gms.maps.model.LatLng;
import java.text.DateFormat;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;
import java.util.Locale;

import static android.content.Context.ACTIVITY_SERVICE;

/**
 * Created by Shaban on 11/17/2017.
 * Support class for general operation in general application
 */
public class Utils {

    /**
     * A method to check if dangerous permission is granted and ask for it if not
     * (needed for OS versions higher than Marshmallow)
     * @param activity: the activity that asks for a specific permission
     * @param permission: the asked permission to be granted
     * @param requestCode: a code of the request to be handled in the activity
     * @return true if permission is granted, or false otherwise
     */
    public static boolean isPermissionGranted(Activity activity, String permission, int requestCode) {
        if (Build.VERSION.SDK_INT >= 23) {
            if (ContextCompat.checkSelfPermission(activity, permission) == PackageManager.PERMISSION_GRANTED) {
                return true;
            } else {
                ActivityCompat.requestPermissions(activity, new String[]{permission}, requestCode);
                return false;
            }
        } else {
            return true;
        }
    }

    /**
     * A method to check the state of journey recording
     * @param preferences: Shared Preferences instance to read/write permanent values
     * @return true if there's a journey that is recording now, or false otherwise
     */
    public static boolean isTrackingOn(SharedPreferences preferences) {
        return preferences.getBoolean("TRACKING", false);
    }

    /**
     *  A method to get the id of a journey that is in recording phase
     * @param preferences: Shared Preferences instance to read/write permanent values
     * @return journey id that is in recording phase, if no recording journeys found the value is 0
     */
    public static long getCurrentJourneyID(SharedPreferences preferences) {
        return preferences.getLong("CURRENT_JOURNEY", 0L);
    }

    /**
     *  A method to get the start timestamp of a journey that is in recording phase
     * @param preferences: Shared Preferences instance to read/write permanent values
     * @return journey start timestamp that is in recording phase, if no recording journeys found the value is 0
     */
    public static long getCurrentJourneyStartDate(SharedPreferences preferences) {
        return preferences.getLong("CURRENT_START_DATE", 0L);
    }

    /**
     * A method to store journey tracking state
     * @param preferences: Shared Preferences instance to read/write permanent values
     * @param state: a boolean value that represents tracking state
     */
    public static void setTracking(SharedPreferences preferences, boolean state) {
        SharedPreferences.Editor editor = preferences.edit();
        editor.putBoolean("TRACKING", state);
        editor.apply();
    }

    /**
     * A method to set a recently started journey id and start timestamp
     * @param preferences: Shared Preferences instance to read/write permanent values
     * @param id: long value that represents the journey id after adding to the database
     * @param timestamp: long value that represents the start timestamp of the journey
     */
    public static void setCurrentJourneyID(SharedPreferences preferences, long id, long timestamp) {
        SharedPreferences.Editor editor = preferences.edit();
        editor.putLong("CURRENT_JOURNEY", id);
        editor.putLong("CURRENT_START_DATE", timestamp);
        editor.apply();
    }

    /**
     * A method to set the last detected location
     * @param preferences: Shared Preferences instance to read/write permanent values
     * @param location: the last detected location
     */
    public static void setLastLocation(SharedPreferences preferences, Location location) {
        SharedPreferences.Editor editor = preferences.edit();
        editor.putString("LAST_LOCATION_LAT", location.getLatitude() + "");
        editor.putString("LAST_LOCATION_LNG", location.getLongitude() + "");
        editor.apply();
    }

    /**
     * A method to get the last detected location
     * @param preferences: Shared Preferences instance to read/write permanent values
     * @return LatLng object that represents the last detected location
     */
    public static LatLng getLastLocation(SharedPreferences preferences) {
        double lat = Double.parseDouble(preferences.getString("LAST_LOCATION_LAT", "200"));
        double lng = Double.parseDouble(preferences.getString("LAST_LOCATION_LNG", "200"));
        if (lat < 200 && lng < 200) {
            return new LatLng(lat, lng);
        }
        return null;
    }

    /**
     * A method to check if GPS is enabled or not
     * @param locationManager: used to check the state of gps services and network services for location
     * @return true if GPS is enabled, false otherwise
     */
    public static boolean isGPSEnabled(LocationManager locationManager) {
        return locationManager.isProviderEnabled(LocationManager.GPS_PROVIDER);
    }

    /**
     * A method to check if Wifi location is enabled or not
     * @param locationManager: used to check the state of gps services and network services for location
     * @return true if Wifi location is enabled, false otherwise
     */
    public static boolean isNetworkEnabled(LocationManager locationManager) {
        return locationManager.isProviderEnabled(LocationManager.NETWORK_PROVIDER);
    }

    /**
     * A method to format a date in a specific formula
     * @param date: Date object to be formatted
     * @return A formatted String of the date
     */
    public static String formatDate(@NonNull Date date) {
        DateFormat dateFormat = new SimpleDateFormat("MM/dd/yyyy HH:mm:ss", Locale.ENGLISH);
        return dateFormat.format(date);
    }

    /**
     * A method to run a method on the main thread, it is used from other threads
     * @param context: represents the current executor
     * @param runnable: An object with method run to be used in other threads
     */
    public static void runOnMainThread(Context context, Runnable runnable) {
        Handler handler = new Handler(context.getMainLooper());
        handler.post(runnable);
    }

    /**
     * A method to add the back button in toolbar
     * @param activity: the activity to be configured
     */
    public static void configureToolbar(AppCompatActivity activity) {
        if (activity.getSupportActionBar() != null) {
            activity.getSupportActionBar().setDisplayHomeAsUpEnabled(true);
            activity.getSupportActionBar().setDisplayShowHomeEnabled(true);
        }
    }

    /**
     * A method to Convert a Location object to LatLng one
     * @param location: Location object to be converted
     * @return converted LatLng object
     */
    public static LatLng getLatLngFromLocation(Location location) {
        return new LatLng(location.getLatitude(), location.getLongitude());
    }

    /**
     * A method to Convert a list of Location objects to list of LatLng ones
     * @param locations: Location list to be converted
     * @return converted LatLng list
     */
    public static List<LatLng> getLatLngsFromLocationPoints(List<LocationPoint> locations) {
        List<LatLng> latLngs = new ArrayList<>();
        if (locations != null) {
            for (int i=0; i<locations.size(); i++) {
                latLngs.add(new LatLng(locations.get(i).getLocation().getLatitude(),
                        locations.get(i).getLocation().getLongitude()));
            }
        }
        return latLngs;
    }

    /**
     * A method to check if TrackerService is running
     * @param context: represents the current context
     * @return true if TrackerService is running, false otherwise
     */
    public static boolean isServiceRunning(Context context) {
        ActivityManager manager = (ActivityManager) context.getSystemService(ACTIVITY_SERVICE);
        if (manager != null) {
            for (ActivityManager.RunningServiceInfo info : manager.getRunningServices(Integer.MAX_VALUE)) {
                if (info.service.getClassName().equals(Constants.SERVICE_CLASS_NAME)) {
                    return true;
                }
            }
        }
        return false;
    }

    /**
     * A method to check if Google Play Services are available on your device to be used with Fused Location
     * @param context: Current executor
     * @return true if Google Play Services are available on your device, false otherwise
     */
    public static boolean isGooglePlayServicesAvailable(Context context) {
        GoogleApiAvailability googleApiAvailability = GoogleApiAvailability.getInstance();
        int status = googleApiAvailability.isGooglePlayServicesAvailable(context);
        return status == ConnectionResult.SUCCESS;
    }

}
