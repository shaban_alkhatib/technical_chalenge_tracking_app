package com.floow.trackme.tools;

import android.arch.persistence.room.TypeConverter;
import java.util.Date;

/**
 * Created by Shaban on 11/16/2017.
 * Converter between Date and String, needed for Room Persistence database
 */
public class DateConverter {

    @TypeConverter
    public static Date toDate(Long timestamp) {
        return timestamp == null ? null : new Date(timestamp);
    }

    @TypeConverter
    public static Long toTimestamp(Date date) {
        return date == null ? null : date.getTime();
    }

}
