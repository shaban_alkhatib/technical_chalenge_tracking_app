package com.floow.trackme.tools;

import android.graphics.Color;

import com.floow.trackme.entities.LocationPoint;
import com.google.android.gms.maps.CameraUpdateFactory;
import com.google.android.gms.maps.GoogleMap;
import com.google.android.gms.maps.model.BitmapDescriptorFactory;
import com.google.android.gms.maps.model.CameraPosition;
import com.google.android.gms.maps.model.CircleOptions;
import com.google.android.gms.maps.model.LatLng;
import com.google.android.gms.maps.model.Marker;
import com.google.android.gms.maps.model.MarkerOptions;
import com.google.android.gms.maps.model.PolylineOptions;
import java.util.List;

/**
 * Created by Shaban on 11/17/2017.
 * Support class for drawing paths and locations on map
 */
public class MapUtils {

    /**
     * A method to add a new location point to the current path
     * @param map: GoogleMap view that can show markers and paths on map
     * @param options: represents the holder of location points to be drawn as a polygon
     * @param location: a new location point to be added to the path
     * @param builder: a builder to configure camera movement on showing a location
     */
    public static void extendPath(GoogleMap map, PolylineOptions options, LatLng location, CameraPosition.Builder builder){
        if(map == null){
            return;
        }
        options.add(location);
        map.addPolyline(options);
        animateCamera(builder, map, location);
    }

    /**
     * A method to draw a path of two location points or more
     * @param map: GoogleMap view that can show markers and paths on map
     * @param options: represents the holder of location points to be drawn as a polygon
     * @param positions: a list of locations as LatLng objects to be drawn as a path
     * @param builder: a builder to configure camera movement on showing a location
     */
    public static void drawPath(GoogleMap map, PolylineOptions options, List<LatLng> positions, CameraPosition.Builder builder){
        if(map == null){
            return;
        }
        if (options.getPoints() != null)
            options.getPoints().clear();
        options.addAll(positions);
        map.addPolyline(options);
        animateCamera(builder, map, positions.get(0));
    }

    /**
     * A method to draw a path of two location points or more
     * @param map: GoogleMap view that can show markers and paths on map
     * @param options: represents the holder of location points to be drawn as a polygon
     * @param positions: a list of locations as LocationPoint objects to be drawn as a path
     * @param builder: a builder to configure camera movement on showing a location
     */
    public static void drawPointsPath(GoogleMap map, PolylineOptions options, List<LocationPoint> positions,
                                CameraPosition.Builder builder){
        if(map == null){
            return;
        }
        if (options.getPoints() != null)
            options.getPoints().clear();
        for (int i=0; i<positions.size(); i++)
            options.add(new LatLng(positions.get(i).getLocation().getLatitude(),
                    positions.get(i).getLocation().getLongitude()));
        map.addPolyline(options);
        animateCamera(builder, map, new LatLng(positions.get(0).getLocation().getLatitude(),
                positions.get(0).getLocation().getLongitude()));
    }

    /**
     * A method to show the current location of user as a magenta circle
     * @param map: GoogleMap view that can show markers and paths on map
     * @param position: represents th location to be shown
     * @param builder: a builder to configure camera movement on showing a location
     */
    public static void showCurrentLocation(GoogleMap map, LatLng position, CameraPosition.Builder builder) {
        if (map != null) {
            map.addCircle(new CircleOptions()
                    .center(position)
                    .radius(3)
                    .strokeColor(Color.MAGENTA)
                    .fillColor(Color.MAGENTA));
            animateCamera(builder, map, position);
        }
    }

    /**
     * A method to show the start location of a journey as a blue circle
     * @param map: GoogleMap view that can show markers and paths on map
     * @param position: represents the location to be shown
     * @param builder: a builder to configure camera movement on showing a location
     */
    public static Marker showStartLocation(GoogleMap map, LatLng position, CameraPosition.Builder builder) {
        Marker marker = null;
        if (map != null) {
            map.addCircle(new CircleOptions()
                    .center(position)
                    .radius(2)
                    .strokeColor(Color.BLUE)
                    .fillColor(Color.BLUE));
            animateCamera(builder, map, position);
            marker = map.addMarker(new MarkerOptions()
                    .position(position)
                    .icon(BitmapDescriptorFactory.defaultMarker(BitmapDescriptorFactory.HUE_AZURE)));
        }
        return marker;
    }
    /**
     * A method to show the end location of a journey as a magenta circle
     * @param map: GoogleMap view that can show markers and paths on map
     * @param position: represents the location to be shown
     */
    public static Marker showEndLocation(GoogleMap map, LatLng position) {
        Marker marker = null;
        if (map != null) {
            map.addCircle(new CircleOptions()
                    .center(position)
                    .radius(3)
                    .strokeColor(Color.MAGENTA)
                    .fillColor(Color.MAGENTA));
            marker = map.addMarker(new MarkerOptions()
                    .position(position)
                     .icon(BitmapDescriptorFactory.defaultMarker(BitmapDescriptorFactory.HUE_AZURE)));
        }
        return marker;
    }


    /**
     * A method to move the screen on map to a specific location
     * @param builder: a builder to configure camera movement on showing a location
     * @param map: GoogleMap view that can show markers and paths on map
     * @param position: represents th location to be shown
     */
    private static void animateCamera(CameraPosition.Builder builder, GoogleMap map, LatLng position){
        if (builder != null) {
            CameraPosition cameraPosition = builder.target(position).build();
            map.animateCamera(CameraUpdateFactory.newCameraPosition(cameraPosition));
        }
    }
}
