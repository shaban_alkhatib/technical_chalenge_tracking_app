package com.floow.trackme.tools;

import android.arch.persistence.room.TypeConverter;
import android.location.Location;
import com.google.gson.Gson;
import com.google.gson.reflect.TypeToken;
import java.lang.reflect.Type;

/**
 * Created by Shaban on 11/16/2017.
 * Converter between Location and JSON, needed for Room Persistence database
 */
public class LocationConverter {

    @TypeConverter
    public static Location toLocation(String location) {
        Type listType = new TypeToken<Location>() {}.getType();
        return new Gson().fromJson(location, listType);
    }

    @TypeConverter
    public static String toString(Location location) {
        Gson gson = new Gson();
        return gson.toJson(location);
    }

}
