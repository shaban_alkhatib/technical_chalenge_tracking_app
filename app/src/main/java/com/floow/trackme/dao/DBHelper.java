package com.floow.trackme.dao;

import android.content.SharedPreferences;
import android.os.AsyncTask;
import android.util.Log;

import com.floow.trackme.entities.Journey;
import com.floow.trackme.entities.LocationPoint;
import com.floow.trackme.listeners.OnJourneyCreateListener;
import com.floow.trackme.tools.Utils;
import java.util.Date;

/**
 * Created by Shaban on 11/18/2017.
 * Support class to do some operations on database using AsyncTasks needed for Room Persistence database
 */

public class DBHelper {

    public static class NewJourneyTask extends AsyncTask<Void, Void, Long> {

        private long startTimestamp;
        private SharedPreferences preferences;
        private TrackerDao dao;
        private OnJourneyCreateListener listener;

        public NewJourneyTask(long startTimestamp, SharedPreferences preferences, TrackerDao dao,
                              OnJourneyCreateListener listener) {
            this.startTimestamp = startTimestamp;
            this.preferences = preferences;
            this.dao = dao;
            this.listener = listener;
        }

        @Override
        protected Long doInBackground(Void... params) {
            return dao.insertJourney(new Journey(new Date(startTimestamp), null));
        }

        @Override
        protected void onPostExecute(Long result) {
            Log.e("TRACKING", "" + result);
            if (result > 0) {
                Utils.setCurrentJourneyID(preferences, result, startTimestamp);
                if (listener != null)
                    listener.onJourneyCreated(result);
            }
        }
    }

    public static class UpdateJourneyTask extends AsyncTask<Void, Void, Void> {

        private Journey journey;
        private SharedPreferences preferences;
        private TrackerDao dao;

        public UpdateJourneyTask(Journey journey, SharedPreferences preferences, TrackerDao dao) {
            this.journey = journey;
            this.preferences = preferences;
            this.dao = dao;
        }

        @Override
        protected Void doInBackground(Void... params) {
            dao.updateJourney(journey);
            return null;
        }

        @Override
        protected void onPostExecute(Void result) {
            Utils.setCurrentJourneyID(preferences, 0, 0);
        }
    }

    public static class NewLocationTask extends AsyncTask<Void, Void, Long> {

        private LocationPoint locationPoint;
        private TrackerDao dao;

        public NewLocationTask(LocationPoint locationPoint, TrackerDao dao) {
            this.locationPoint = locationPoint;
            this.dao = dao;
        }

        @Override
        protected Long doInBackground(Void... params) {
            return dao.insertLocationPoint(locationPoint);
        }
    }

}
