package com.floow.trackme.dao;

import android.arch.persistence.room.Database;
import android.arch.persistence.room.RoomDatabase;
import android.arch.persistence.room.TypeConverters;
import com.floow.trackme.entities.Journey;
import com.floow.trackme.entities.LocationPoint;
import com.floow.trackme.tools.DateConverter;
import com.floow.trackme.tools.LocationConverter;

/**
 * Created by Shaban on 11/16/2017.
 * A class to define Room Persistence database
 */

@Database(entities = {Journey.class, LocationPoint.class}, version = 2, exportSchema = false)
@TypeConverters({DateConverter.class, LocationConverter.class})
public abstract class TrackerDatabase extends RoomDatabase {
    public abstract TrackerDao trackerDao();
}
