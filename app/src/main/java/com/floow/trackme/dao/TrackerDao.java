package com.floow.trackme.dao;

import android.arch.lifecycle.LiveData;
import android.arch.persistence.room.Dao;
import android.arch.persistence.room.Insert;
import android.arch.persistence.room.Query;
import android.arch.persistence.room.Transaction;
import android.arch.persistence.room.TypeConverters;
import android.arch.persistence.room.Update;
import com.floow.trackme.entities.Journey;
import com.floow.trackme.entities.JourneyDetails;
import com.floow.trackme.entities.LocationPoint;
import com.floow.trackme.tools.Constants;
import com.floow.trackme.tools.DateConverter;
import com.floow.trackme.tools.LocationConverter;
import java.util.List;

/**
 * Created by Shaban on 11/16/2017.
 * Interface of available operations on Database
 */

@Dao
@TypeConverters({DateConverter.class, LocationConverter.class})
public interface TrackerDao {

    @Insert
    long insertLocationPoint(LocationPoint locationPoint);

    @Insert
    long insertJourney(Journey journey);

    @Update
    void updateJourney(Journey journey);

    @Query("SELECT * FROM " + Constants.TABLE_JOURNEYS + " WHERE endDate IS NOT NULL ORDER BY id DESC" )
    LiveData<List<Journey>> getJourneys();

    @Transaction
    @Query("SELECT * FROM " + Constants.TABLE_JOURNEYS + " WHERE id=:id" )
    LiveData<JourneyDetails> getJourneyByID(long id);

}
